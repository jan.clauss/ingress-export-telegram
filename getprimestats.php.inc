<?php
//Ü
function getprimestats($stats) {
	global
		$bottoken,$bot_owner;

	$stattxt[]=array('idx' =>  0, 'typ' => 'A', 'txt' => 'Time Span');
	$stattxt[]=array('idx' =>  1, 'typ' => 'A', 'txt' => 'Agent Name');
	$stattxt[]=array('idx' =>  2, 'typ' => 'A', 'txt' => 'Agent Faction');
	$stattxt[]=array('idx' =>  3, 'typ' => 'A', 'txt' => 'Date (yyyy-mm-dd)');
	$stattxt[]=array('idx' =>  4, 'typ' => 'A', 'txt' => 'Time (hh:mm:ss)');
	$stattxt[]=array('idx' =>  5, 'typ' => 'N', 'txt' => 'Level');
	$stattxt[]=array('idx' =>  6, 'typ' => 'N', 'txt' => 'Lifetime AP');
	$stattxt[]=array('idx' =>  7, 'typ' => 'N', 'txt' => 'Current AP');
	$stattxt[]=array('idx' =>  8, 'typ' => 'N', 'txt' => 'Unique Portals Visited', 'badges' => array('name' => 'Explorer', 100, 1000, 2000, 10000, 30000));
	$stattxt[]=array('idx' => 47, 'typ' => 'N', 'txt' => 'Unique Portals Drone Visited');
	$stattxt[]=array('idx' => 48, 'typ' => 'N', 'txt' => 'Furthest Drone Flight Distance');
	$stattxt[]=array('idx' =>  9, 'typ' => 'N', 'txt' => 'Portals Discovered');
	$stattxt[]=array('idx' => 10, 'typ' => 'N', 'txt' => 'Seer Points', 'badges' => array('name' => 'Seer', 10, 50, 200, 500, 5000));
	$stattxt[]=array('idx' => 11, 'typ' => 'N', 'txt' => 'XM Collected');
	$stattxt[]=array('idx' => 12, 'typ' => 'N', 'txt' => 'OPR Agreements', 'badges' => array('name' => 'Recon', 100, 750, 2500, 5000, 10000));
	$stattxt[]=array('idx' => 46, 'typ' => 'N', 'txt' => 'Portal Scans Uploaded', 'badges' => array('name' => 'Scout', 50, 250, 1000, 3000, 6000));
	$stattxt[]=array('idx' => 51, 'typ' => 'N', 'txt' => 'Scout Controller on Unique Portals');
	$stattxt[]=array('idx' => 52, 'typ' => 'N', 'txt' => 'Battle Beacon Combatant');
	$stattxt[]=array('idx' => 13, 'typ' => 'N', 'txt' => 'Distance Walked', 'badges' => array('name' => 'Trekker', 10, 100, 300, 1000, 2500));
	$stattxt[]=array('idx' => 53, 'typ' => 'N', 'txt' => 'Kinetic Capsules Completed'); // 2020-10-19
	$stattxt[]=array('idx' => 14, 'typ' => 'N', 'txt' => 'Resonators Deployed', 'badges' => array('name' => 'Builder', 2000, 10000, 30000, 100000, 200000));
	$stattxt[]=array('idx' => 15, 'typ' => 'N', 'txt' => 'Links Created', 'badges' => array('name' => 'Connector', 50, 1000, 5000, 25000, 100000));
	$stattxt[]=array('idx' => 16, 'typ' => 'N', 'txt' => 'Control Fields Created', 'badges' => array('name' => 'Mind Controller', 100, 500, 2000, 10000, 40000));
	$stattxt[]=array('idx' => 17, 'typ' => 'N', 'txt' => 'Mind Units Captured', 'badges' => array('name' => 'Illuminator', 5000, 50000, 250000, 1000000, 4000000));
	$stattxt[]=array('idx' => 18, 'typ' => 'N', 'txt' => 'Longest Link Ever Created');
	$stattxt[]=array('idx' => 19, 'typ' => 'N', 'txt' => 'Largest Control Field');
	$stattxt[]=array('idx' => 20, 'typ' => 'N', 'txt' => 'XM Recharged', 'badges' => array('name' => 'Recharcher', 1000000, 10000000, 30000000, 100000000, 250000000));
	$stattxt[]=array('idx' => 21, 'typ' => 'N', 'txt' => 'Portals Captured', 'badges' => array('name' => 'Liberator', 100, 1000, 5000, 15000, 40000));
	$stattxt[]=array('idx' => 22, 'typ' => 'N', 'txt' => 'Unique Portals Captured', 'badges' => array('name' => 'Pioneer', 20, 200, 1000, 5000, 20000));
	$stattxt[]=array('idx' => 23, 'typ' => 'N', 'txt' => 'Mods Deployed', 'badges' => array('name' => 'Ingeneer', 150, 1500, 5000, 20000, 50000));
	$stattxt[]=array('idx' => 24, 'typ' => 'N', 'txt' => 'Resonators Destroyed', 'badges' => array('name' => 'Purifier', 2000, 10000, 30000, 100000, 300000));
	$stattxt[]=array('idx' => 25, 'typ' => 'N', 'txt' => 'Portals Neutralized');
	$stattxt[]=array('idx' => 26, 'typ' => 'N', 'txt' => 'Enemy Links Destroyed');
	$stattxt[]=array('idx' => 27, 'typ' => 'N', 'txt' => 'Enemy Fields Destroyed');
	$stattxt[]=array('idx' => 52, 'typ' => 'N', 'txt' => 'Battle Beacon Combatant');  //20201002
	$stattxt[]=array('idx' => 28, 'typ' => 'N', 'txt' => 'Max Time Portal Held', 'badges' => array('name' => 'Guardian', 3, 10, 20, 90, 150));
	$stattxt[]=array('idx' => 29, 'typ' => 'N', 'txt' => 'Max Time Link Maintained');
	$stattxt[]=array('idx' => 30, 'typ' => 'N', 'txt' => 'Max Link Length x Days');
	$stattxt[]=array('idx' => 31, 'typ' => 'N', 'txt' => 'Max Time Field Held');
	$stattxt[]=array('idx' => 32, 'typ' => 'N', 'txt' => 'Largest Field MUs x Days');
	$stattxt[]=array('idx' => 50, 'typ' => 'N', 'txt' => 'Forced Drone Recalls');
	$stattxt[]=array('idx' => 33, 'typ' => 'N', 'txt' => 'Unique Missions Completed', 'badges' => array('name' => 'SpecOps', 5, 25, 100, 200, 500));
	$stattxt[]=array('idx' => 34, 'typ' => 'N', 'txt' => 'Hacks', 'badges' => array('name' => 'Hacker', 2000, 10000, 30000, 100000, 200000));
	$stattxt[]=array('idx' => 49, 'typ' => 'N', 'txt' => 'Drone Hacks');
	$stattxt[]=array('idx' => 35, 'typ' => 'N', 'txt' => 'Glyph Hack Points', 'badges' => array('name' => 'Translator', 200, 2000, 6000, 20000, 50000));
	$stattxt[]=array('idx' => 36, 'typ' => 'N', 'txt' => 'Longest Hacking Streak', 'badges' => array('name' => 'Sojourner', 15, 30, 60, 180, 360));
	$stattxt[]=array('idx' => 54, 'typ' => 'N', 'txt' => 'Months Subscribed');
//	$stattxt[]=array('idx' => 37, 'typ' => 'N', 'txt' => 'Agents Successfully Recruited', 'badges' => array('name' => 'Recruiter', 2, 10, 25, 50, 100));
	$stattxt[]=array('idx' => 37, 'typ' => 'N', 'txt' => 'Agents Recruited', 'badges' => array('name' => 'Recruiter', 2, 10, 25, 50, 100));
	$stattxt[]=array('idx' => 38, 'typ' => 'N', 'txt' => 'Mission Day(s) Attended', 'badges' => array('name' => 'Mission Day', 1, 3, 6, 10, 20));
	$stattxt[]=array('idx' => 39, 'typ' => 'N', 'txt' => 'NL-1331 Meetup(s) Attended', 'badges' => array('name' => 'NL-1331 Meetups', 1, 5, 10, 25, 50));
	$stattxt[]=array('idx' => 40, 'typ' => 'N', 'txt' => 'First Saturday Events', 'badges' => array('name' => 'First Saturday', 1, 6, 12, 24, 36));
	$stattxt[]=array('idx' => 41, 'typ' => 'N', 'txt' => 'Clear Fields Events');
	$stattxt[]=array('idx' => 42, 'typ' => 'N', 'txt' => 'OPR Live Events');
	$stattxt[]=array('idx' => 43, 'typ' => 'N', 'txt' => 'Prime Challenges');
	$stattxt[]=array('idx' => 44, 'typ' => 'N', 'txt' => 'Stealth Ops Missions');
//	$stattxt[]=array('idx' => 101, 'typ' => 'N', 'txt' => 'Umbra: Unique Resonator Slots Deployed'); // Dez. 2019
//	$stattxt[]=array('idx' => 102, 'typ' => 'N', 'txt' => 'Didact: Total Fields Created'); // Feb 2020
//	$stattxt[]=array('idx' => 103, 'typ' => 'N', 'txt' => 'Didact Fields Created'); // Feb 2020 und mitten im Event geändert
	$stattxt[]=array('idx' => 45, 'typ' => 'N', 'txt' => 'Recursions');
	
	
	// Es wird davon ausgegangen, dass sich die Reihenfolge nicht ändert, es können nur Werte fehlen, oder kommen welche dazu
	// Neue müssen an der richtigen Position oben eingefügt werden, idx sollte hochgezählt werden, dass die im Internen Array am Ende sind
	
	
	if(substr($stats,0,34)<>'Time Span Agent Name Agent Faction') {
		error_log("Falsche Stats");
		return(array(1, 'Das sind keine Stats'));
	}
	$linestats=explode("\n",trim($stats));
	$values=explode(" ",$linestats[1]); // Werte als Array
	$linestats=trim($linestats[0]); // Statistiknamen als zusammenhängender String
	
	// Testen ob Datum an der richtigen Stelle ("GESAMT" kann in anderen Sprachen aus mehreren Wörtern bestehen)
	$i=0;
	while($values[3+$i]<>date('Y-m-d',strtotime($values[3+$i])) and $i < 6) $i++;
	if($values[3+$i]<>date('Y-m-d',strtotime($values[3+$i]))) {
		// Kein korrektes Datum gefunden
		return(array(1, "Konnte Stats nicht auswerten"));
	}
	for ($j=1; $j<=$i; $j++) { // Datum war nicht an der richtigen Stelle jetzt entsprechend die ersten Einträge des Array zusammen führen
		$values[0]=$values[0]." ".$values[$j];
		unset($values[$j]);
	}
	if($i>0) $values=array_values($values); // Array neu Indizieren
	// Ende: Testen ob Datum an der richtigen Stelle

	$valuepos=0;
	$arraypos=0; // 
	$nextunbekannteintrag=true;
	while($linestats<>'' and IsSet($stattxt[$arraypos])) {
//		echo "z:$z, arraypos: $arraypos<br>";
		if(substr($linestats,0,strlen($stattxt[$arraypos]['txt'])) == $stattxt[$arraypos]['txt']) {
			// Eintrag in der Bezeichnungszeile gefunden
			if($stattxt[$arraypos]['typ'] == 'N' and strpos($values[$valuepos],',') !== false) { // Numerischer Wert und es kommt ein ',' in value vor - 8.5.20 Fehler in deutsche iOS Version
				$values[$valuepos]=substr_replace($values[$valuepos],'',strpos($values[$valuepos],','),1);
			}
			$retarr[$stattxt[$arraypos]['idx']]=array('idx' => $stattxt[$arraypos]['idx'], 'value' => $values[$valuepos], 'txt' => $stattxt[$arraypos]['txt'], 'typ' => $stattxt[$arraypos]['typ']);
	//		echo substr($linestats,0,40)."<br>".$stattxt[$arraypos]['txt']." - ".$values[$valuepos]."<br>";
			$linestats=trim(substr($linestats,strlen($stattxt[$arraypos]['txt'])));
			$valuepos++;
			$arraypos++;
			$nextunbekannteintrag=true;
		}else{
			// Nicht gefunden
			// Prüfen, ob Neu, melden und überspringen
			$i=1;
			while(IsSet($stattxt[$arraypos+$i]) and substr($linestats,0,strlen($stattxt[$arraypos+$i]['txt']))<>$stattxt[$arraypos+$i]['txt']) {
				$i++;
			}
//			echo "i: $i, ".($i+$arraypos)."<br>";
			if(!IsSet($stattxt[$arraypos+$i])) { // Nicht gefunden
				// Wert nicht gefunden
//				echo "Unbekannter Eintrag<br>".substr($linestats,0,20)."<br>";
				// Ein "Wort" löschen und nächste Runde
				if($nextunbekannteintrag) {
					$valuepos++; // Diesen Eintrag überspringen
				// Melden, Text speichern
					$answer="Unbekannter Eintrag\n".substr($linestats,0,20);
					tg_send($bottoken,$bot_owner,$answer);
					$answer=$stats;
					tg_send($bottoken,$bot_owner,$answer);
				}
				$nextunbekannteintrag=false;
				$linestats=trim(substr($linestats,strpos($linestats, ' ')));
//				echo substr($linestats,0,40)."<br>";
			}else{ // gefunden
				$arraypos=$arraypos+$i;
			}
		}
	}
	return($retarr);
}
?>