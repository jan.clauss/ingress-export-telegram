# Ingress-Export Telegram

Beim Export der Ingress-Statistiken zu Telegram gehen die Tabulatoren als Trenner verloren.
Diese Klasse wertet die Statistik aufgrund der bekannten Bezeichnungen aus und gibt sie als Array zurück.
Dabei werden auch neue Einträge erkannt und als Warnung in der Ausgabe mit übergeben

